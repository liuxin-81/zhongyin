package com.lx.service.impl;

import com.lx.bean.Admin;
import com.lx.mapper.LoginMapper;
import com.lx.service.LoginService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class LoginServiceImpl implements LoginService {
    @Resource
    LoginMapper loginMapper;


    @Override
    public Admin select(String name, String pwd) {
        System.out.println(name+"service" +
                "");
        return loginMapper.login(name,pwd);
    }
}
