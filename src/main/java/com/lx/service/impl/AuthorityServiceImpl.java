package com.lx.service.impl;

import com.lx.bean.Authority;
import com.lx.mapper.AuthorityMapper;
import com.lx.service.AuthorityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class AuthorityServiceImpl implements AuthorityService {
    @Resource
    AuthorityMapper authorityMapper ;

    @Override
    public List getHome(Integer id) {
        return authorityMapper.getHome(id);
    }
}
