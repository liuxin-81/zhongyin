package com.lx.service.impl;

import com.lx.bean.Role;
import com.lx.mapper.RoleMapper;
import com.lx.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    RoleMapper roleMapper;
    @Override
    public List<Map> select() {
        return roleMapper.select();
    }

    @Override
    public int add(Role role) {
        return roleMapper.add(role);
    }

    @Override
    public int del(Integer id) {
        return roleMapper.del(id);
    }

    @Override
    public int upd(Role role) {
        return roleMapper.upd(role);
    }
}
