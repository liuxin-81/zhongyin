package com.lx.service;

import com.lx.bean.Role;

import java.util.List;
import java.util.Map;

public interface RoleService {
    List<Map> select();

    int add(Role role);

    int del(Integer id);

    int upd(Role role);
}
