package com.lx.service;

import com.lx.bean.Admin;

import java.util.Map;

public interface LoginService {
    Admin select(String name, String pwd);
}
