package com.lx.controller;

import com.lx.bean.Admin;
import com.lx.service.LoginService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Resource()
    LoginService loginService;


    @RequestMapping("/login")
    @ResponseBody
    public String login(String name, String pwd, String code, HttpSession session){
        System.out.println(name+"    "+pwd+"     "+code);
        String code2=(String)session.getAttribute("code");
        System.out.println("后台code"+code2);
        Admin map=loginService.select(name,pwd);
        return "200";
    }

}
