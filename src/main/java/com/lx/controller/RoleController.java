package com.lx.controller;

import cn.hutool.json.JSONUtil;
import com.lx.bean.Role;
import com.lx.service.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/Role")
public class RoleController {
        @Resource
    RoleService roleService;
    @RequestMapping("/select")
    @ResponseBody
    public String select(){
        System.out.println("查询全部角色");
        List<Map> list= roleService.select();
        String s = JSONUtil.toJsonStr(list);
        System.out.println(s);
        return s;
    }
    @RequestMapping("/add")
    @ResponseBody
    public String add(Role role){
        System.out.println("添加角色");
        int i= roleService.add(role);
        if(i>0){
            System.out.println("添加成功");
            return "200";
        }else{
            System.out.println("添加失败");
            return "0";
        }
    }
    @RequestMapping("/del")
    @ResponseBody
    public String del(Integer id){
        System.out.println("删除角色");
        int i= roleService.del(id);
        if(i>0){
            System.out.println("删除成功");
            return "200";
        }else{
            System.out.println("删除失败");
            return "0";
        }
    }
    @RequestMapping("/update")
    @ResponseBody
    public String del(Role role){
        System.out.println("修改角色");
        int i= roleService.upd(role);
        if(i>0){
            System.out.println("修改成功");
            return "200";
        }else{
            System.out.println("修改失败");
            return "0";
        }
    }

}
