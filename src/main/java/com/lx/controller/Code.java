package com.lx.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/Code")
public class Code extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CircleCaptcha cap= CaptchaUtil.createCircleCaptcha(200,100,4,10);
        String code=cap.getCode();
        req.getSession().setAttribute("code",code);
        cap.write(resp.getOutputStream());
    }
}
