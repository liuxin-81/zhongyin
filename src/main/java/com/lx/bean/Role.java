package com.lx.bean;

import lombok.Data;

import java.util.List;

@Data
public class Role {
    private Integer id;
    private String name;
    private String remark;
    private List<Admin> rid;
}
