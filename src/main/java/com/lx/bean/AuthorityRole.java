package com.lx.bean;
import lombok.Data;

import java.util.List;

@Data
public class AuthorityRole {
  private Integer id;
  private Integer authority_id;
  private Integer role_id;
  private List<Authority> autid;
}