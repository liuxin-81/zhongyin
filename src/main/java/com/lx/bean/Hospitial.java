package com.lx.bean;
import lombok.Data;
@Data
public class Hospitial {
  private Integer id;
  private String name;
  private String address;
  private Integer state;
  private String remark;

}