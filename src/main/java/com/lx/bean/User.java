package com.lx.bean;
import lombok.Data;

import java.util.Date;

@Data
public class User {
  private Integer id;
  private String name;
  private String tel;
  private String email;
  private String password;
  private Date create_time;
  private Integer state;
  private Integer admin_id;
  private Integer is_delete;
  private Integer role_id;
  private Integer hospitial_id;
  private String remark;

}