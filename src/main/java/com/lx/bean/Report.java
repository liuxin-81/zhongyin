package com.lx.bean;
import lombok.Data;

import java.util.Date;

@Data
public class Report {
  private Integer id;
  private String number;
  private Integer patient_id;
  private Integer doctor_id;
  private String path;
  private Date create_time;
  private Integer type;
  private String result;
  private Integer hospitial_id;
  private Integer update_id;
  private Date update_time;
  private String remark;
  private Integer start;

}