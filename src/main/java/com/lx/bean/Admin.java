package com.lx.bean;

import lombok.Data;

import java.util.List;

@Data
public class Admin {
    private Integer id;
    private String name;
    private String tel;
    private String email;
    private String password;
    private String create_time;
    private Integer state;
    private Integer admin_id;
    private Integer is_delete;
    private Integer role_id;
    private Integer hospitial_id;
    private String remark;
    private Integer authority_id;
    private List<Authority> authorities;
}
