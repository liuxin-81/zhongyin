package com.lx.bean;
import lombok.Data;

import java.util.List;

@Data
public class Authority {
  private Integer id;
  private String menu_name;
  private String menu_icon;
  private String name;
  private String url;
  private Integer parent_id;
  private Integer state;
  private Integer row_number;
  private String remark;
  private  AuthorityRole atid;
  private Integer aid;
}