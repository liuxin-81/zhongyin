package com.lx.mapper;

import com.lx.bean.Admin;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public interface LoginMapper {
        Admin login(String name, String pwd);

}
