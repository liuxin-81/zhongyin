package com.lx.mapper;

import com.lx.bean.Role;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface RoleMapper {
    public List<Map> select();

    int add(Role role);

    int del(Integer id);

    int upd(Role role);
}
